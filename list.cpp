#include <iostream>
#include "list.h"
using namespace std;

List::~List()
{
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node *temp = new Node(el);

	if (tail == 0)
	{
		tail = temp;
		head = tail;
	}

	tail->next = temp;
	tail = temp;
}
char List::popHead()
{
	char el = head->data;
	Node* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node* tmp = head;
	Node* tmp2 = head;

	tmp = tail;
	while (tmp2->next != tail)
	{
		tmp2 = tmp2->next;
	}

	tmp2->next = 0;
	tail = tmp2;

	delete tmp;
	return NULL;
}
bool List::search(char el)
{
	bool search = false;

	Node *pointer = head;

	if (head->next == NULL)
	{
		if (pointer->data == el)
		{
			search = true;
		}
	}
	else
	{
		while (pointer->next != NULL)
		{
			if (pointer->data == el)
			{
				search = true;
				break;
			}
			pointer = pointer->next;
		}

	}
	return search;
}
void List::reverse()
{
	//Create 2 pointer next to head node
	Node* h1 = head->next;
	Node* h2 = head->next->next;

	//Swap head to tail node
	head->next = 0;
	tail = head;

	//Link node from h1 pointer and h2 to check next node are pointed to null or not
	do
	{
		h1->next = head;
		head = h1;
		h1 = h2;
		if (h2 != 0)
		{
			h2 = h1->next;
		}
	} while (h1 != 0);
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}