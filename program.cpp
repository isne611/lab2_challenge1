#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToTail('k');
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.print();
	cout << endl;
	mylist.reverse();
	mylist.print();
	cout << endl;
	if (mylist.search('e') == true)
	{
		cout << "Founded";
	};
	system("PAUSE");

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!

}